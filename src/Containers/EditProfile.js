import React, { Component } from "react";
import {
  Layout,
  Menu,
  Button,
  Divider,
  Icon,
  Row,
  Col,
  Form,
  Card,
  Input,
  Modal,
  Avatar
} from "antd";

import bbaa from "../images.jpg";

const { Header, Content, Footer, Sider } = Layout;
const menus = ["home", "favorite", "cart", "profile"];

class Editprofile extends Component {
  state = {
    collapsed: false,
    imageUrl: "",
    loading: false,
    visible: false
  };

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  goToProfile = () => {
    this.props.history.push("/profile");
  };

  goToMain = () => {
    this.props.history.push("/main");
  };

  goToLogin = () => {
    this.props.history.push("/");
  };

  goToEditProfile = () => {
    this.props.history.push("/editprofile");
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  render() {
    const { visible, loading } = this.state;
    return (
      <div>
        <Layout style={{ width: "100vw", height: "100vh" }}>
          <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
            <h1
              style={{ textAlign: "center", fontSize: "150%", color: "white" }}
            >
              AWARE
            </h1>
            <div className="logo" />
            <Menu theme="dark" mode="inline" defaultSelectedKeys={["2"]}>
              <Menu.Item key="1" onClick={this.goToMain}>
                {/* <Icon style={{ color: "rgb(233,231,232)" }} type="home" /> */}
                <Icon
                  style={{ fontSize: "18px" }}
                  type="home"
                  theme="twoTone"
                />
                <span style={{ fontSize: "18px" }}>Home</span>
              </Menu.Item>
              <Menu.Item key="2" onClick={this.goToProfile}>
                <Icon
                  style={{ fontSize: "18px" }}
                  type="smile"
                  theme="twoTone"
                />
                {/* <Icon style={{ color: "rgb(233,231,232)" }} type="user" /> */}
                <span style={{ fontSize: "18px" }}>Profile</span>
              </Menu.Item>
              <Menu.Item key="3" onClick={this.goToLogin}>
                <Icon
                  style={{ fontSize: "18px" }}
                  type="rocket"
                  theme="twoTone"
                />
                {/* <Icon style={{ color: "rgb(233,231,232)" }} type="upload" /> */}
                <span style={{ fontSize: "18px" }}>Logout</span>
              </Menu.Item>
            </Menu>
          </Sider>

          <Layout
            style={{
              backgroundImage:
                "linear-gradient(to top left,#91D4C2,#45BB89, #3D82AB)"
            }}
          >
            <Header style={{ backgroundImage: `url(${bbaa})`, padding: 0 }}>
              <Icon
                style={{ marginLeft: "2%" }}
                className="trigger"
                type={this.state.collapsed ? "menu" : "menu"}
                onClick={this.toggle}
              />
            </Header>
            <Content
              style={{
                margin: "24px 16px",
                padding: 24,
                background: "#fff",
                minHeight: 280
              }}
            >
              <Row>
                <Col span={6}></Col>
                <Col span={12}>
                  <Card
                    style={{ backgroundColor: "white", textAlign: "center" }}
                  >
                    <h1>Edit Profile</h1>
                    <Avatar size={200} src={this.state.imageUrl} />
                    <Form
                      onSubmit={this.onSubmitFormLogin}
                      style={{ marginTop: "5%" }}
                    >
                      <Form.Item>
                        <Input
                          size="large"
                          prefix={<Icon type="mail" />}
                          placeholder="Email"
                        />
                      </Form.Item>

                      <Form.Item>
                        <div>
                          <Row>
                            <Col span={11}>
                              <Input
                                size="large"
                                prefix={<Icon type="user" />}
                                placeholder="First Name"
                              />
                            </Col>
                            <Col span={2}></Col>
                            <Col span={11}>
                              <Input size="large" placeholder="Last Name" />
                            </Col>
                          </Row>
                        </div>
                      </Form.Item>

                      <Divider />

                      <Form.Item>

                        <a onClick={this.showModal}>
                          Change Password
                          </a>


                      </Form.Item>

                      <Modal
                        visible={visible}
                        title="Change Password"
                        onCancel={this.handleCancel}
                        footer={[
                          <Button
                            key="submit"
                            type="primary"
                            onClick={this.handleCancel}
                          >
                            Submit
                          </Button>,
                          <Button key="back" onClick={this.handleCancel}>
                            Cancel
                          </Button>
                        ]}
                      >
                        <Form onSubmit={this.onSubmitFormLogin}>
                          <Form.Item>
                            <Input
                              size="large"
                              style={{ backgroundColor: "rgba(0, 0, 0, 0.1)" }}
                              prefix={<Icon type="lock" />}
                              type="password"
                              placeholder="Password"
                            />
                          </Form.Item>
                          <Form.Item>
                            <Input
                              size="large"
                              style={{ backgroundColor: "rgba(0, 0, 0, 0.1)" }}
                              prefix={<Icon type="lock" />}
                              type="password"
                              placeholder="Confirm Password"
                            />
                          </Form.Item>
                        </Form>
                      </Modal>

                      <Form.Item>
                        <Button
                          style={{ width: "30%" }}
                          type="primary"
                          htmlType="submit"
                          onClick={this.goToProfile}
                        >
                          Submit
                        </Button>
                        <Button
                          style={{ width: "30%", marginLeft: "10%" }}
                          onClick={this.goToProfile}
                        >
                          Cancel
                        </Button>
                      </Form.Item>
                    </Form>
                  </Card>
                </Col>
                <Col span={6}></Col>
              </Row>
            </Content>
          </Layout>
        </Layout>
      </div>
    );
  }
}

export default Editprofile;

import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Login from './Login'
import Register from './Register'
import Main from './Main'
import Profile from './Profile'
import Editprofile from './Editprofile'
import aa from './aa'
import Fufu from './Fufu'


function Routes(){
    return (
        <div style={{ width: '100%' }}>
            <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/main" component={Main} />
                <Route exact path="/profile" component={Profile} />
                <Route exact path="/editprofile" component={Editprofile} />
                <Route exact path="/aa" component={aa} />
                <Route exact path="/fufu" component={Fufu} />
            </Switch>
        </div>
    )

}

export default Routes

import React, { Component } from "react";
import {
  Form,
  Button,
  Input,
  Icon,
  Divider,
  message,
  Modal,
  Card,
  Col,
  Row
} from "antd";

import bbaa from '../images.jpg'

class Register extends Component {
  goToLogin = () => {
    this.props.history.push("/");
  };
  render() {
    return (
      <div style={{ width: "80%", margin: "10%", textAlign: "center" }}>
        <Row>
          <Col span={6}></Col>
          <Col span={12}>
            <Card style={{ backgroundImage: `url(${bbaa})` }}>
            <h1 style={{color:'white'}}>REGISTER</h1>
            </Card>
            <Card style={{ backgroundColor: "white" }}>

              <Form onSubmit={this.onSubmitFormLogin}>
                <Form.Item>
                  <div>
                    <Row>
                      <Col span={11}>
                        <Input
                          size="large"
                          prefix={<Icon type="user" />}
                          placeholder="First Name"
                        />
                      </Col>
                      <Col span={2}></Col>
                      <Col span={11}>
                        <Input size="large" placeholder="Last Name" />
                      </Col>
                    </Row>
                  </div>
                </Form.Item>

                <Form.Item>
                  <Input
                    size="large"
                    prefix={<Icon type="mail" />}
                    placeholder="Email"
                  />
                </Form.Item>

                <Form.Item>
                  <div>
                    <Row>
                      <Col span={11}>
                        <Input
                          size="large"
                          prefix={<Icon type="lock" />}
                          placeholder="Password"
                          type="password"
                        />
                      </Col>
                      <Col span={2}></Col>
                      <Col span={11}>
                        <Input size="large" placeholder="Confirm Password" type="password" />
                      </Col>
                    </Row>
                  </div>
                </Form.Item>

                <Divider />

                <Form.Item>
                  <Button
                    style={{ width: "40%" }}
                    type="primary"
                    htmlType="submit"
                    onClick={this.goToLogin}
                  >
                    Register
                  </Button>
                  <Button style={{ width: "40%" , marginLeft:'10%' }}>Clear</Button>
                </Form.Item>
              </Form>
            </Card>
          </Col>
          <Col span={6}></Col>
        </Row>
      </div>
    );
  }
}

export default Register;

import React, { Component } from "react";
import {
  Form,
  Button,
  Input,
  Icon,
  Divider,
  Card,
  Col,
  Row,
  Checkbox
} from "antd";

import bbaa from '../images.jpg'
import df from '../122.jpg'

class aa extends Component {
  state = {
    isLoad: false,
    email: "",
    password: "",
    isShowModal: false,
    isLogin: false
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem("user-data");
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
    if (isLoggedIn) {
      this.navigateToMainPage();
    }
  }

  saveInformationUser = email => {
    localStorage.setItem(
      "user-data",
      JSON.stringify({
        email: email,
        isLoggedIn: true,
        imageUrl: this.state.imageUrl
      })
    );
    this.setState({ isLoad: false });
    this.navigateToMainPage();
  };

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  onPasswordChange = event => {
    const password = event.target.value;
    this.setState({ password });
  };

  navigateToMainPage = () => {
    const { history } = this.props;
    history.push("/main");
  };

  goToRegister = () => {
    this.props.history.push("/register");
  };

  goToMain = () => {
    this.props.history.push("/main");
  };

  goToFufu = () => {
    this.props.history.push("/fufu")
  }

  render() {
    return (
      // <div class="container">
      //     <Row>
      //         <Col span={2}></Col>
      //         <Col span={10} style={{textAlign:'center'}}>
      //         <h2>Login Now</h2>
      //         <Form onSubmit={this.onSubmitFormLogin}>
      //             <Form.Item>
      //               <Input
      //                 size="large"
      //                 style={{ backgroundColor: "rgba(0, 0, 0, 0.1)" }}
      //                 prefix={<Icon type="mail" />}
      //                 placeholder="Email"
      //               />
      //             </Form.Item>
      //             <Form.Item>
      //               <Input
      //                 size="large"
      //                 style={{ backgroundColor: "rgba(0, 0, 0, 0.1)" }}
      //                 prefix={<Icon type="lock" />}
      //                 type="password"
      //                 placeholder="Password"
      //               />
      //             </Form.Item>
      //             <Form.Item>
      //               <Checkbox>Remember me</Checkbox>
      //               <Divider type="vertical" />
      //               <a className="login-form-forgot" onClick={this.goToFufu}> Forgot password</a>
      //             </Form.Item>
      //             <Form.Item>
      //               <Button
      //                 style={{ width: "50%" }}
      //                 htmlType="submit"
      //                 onClick={this.goToMain}
      //               >
      //                 Login
      //               </Button>
      //             </Form.Item>
      //             Or <a onClick={this.goToRegister}>register now!</a>
      //           </Form>
      //         </Col>
      //         <Col span={12}>
      //         <img src={df} alt={"df"} style={{width:'100%',height:'100%'}}/> 
      //         </Col>
      //         {/* <Col span={2}></Col> */}
      //     </Row>
      // </div>
      <div>
        <Row>
          <Col span={2}></Col>
          <Col span={10}>
            <div style={{ backgroundColor: 'white', textAlign: 'center', height: '60vh' }}>
              <img width='100%' height='100%' src={df} alt={df}/>
            </div></Col>

          <Col span={10}>
            <div style={{ backgroundColor: 'white', textAlign: 'center', height: '60vh' }}>
              <h1>Login</h1>
              <Form onSubmit={this.onSubmitFormLogin}>
                <Form.Item>
                  <Input
                    size="large"
                    style={{ backgroundColor: "rgba(0, 0, 0, 0.1)" }}
                    prefix={<Icon type="mail" />}
                    placeholder="Email"
                  />
                </Form.Item>
                <Form.Item>
                  <Input
                    size="large"
                    style={{ backgroundColor: "rgba(0, 0, 0, 0.1)" }}
                    prefix={<Icon type="lock" />}
                    type="password"
                    placeholder="Password"
                  />
                </Form.Item>
                <Form.Item>
                  <Checkbox>Remember me</Checkbox>
                  <Divider type="vertical" />
                  <a className="login-form-forgot" onClick={this.goToFufu}> Forgot password</a>
                </Form.Item>
                <Form.Item>
                  <Button
                    style={{ width: "50%" }}
                    htmlType="submit"
                    onClick={this.goToMain}
                  >
                    Login
                  </Button>
                </Form.Item>
                Or <a onClick={this.goToRegister}>register now!</a>
              </Form>
            </div></Col>
          <Col span={2}></Col>
        </Row>

      </div>
    );
  }
}

export default aa;
